# ------list s3 objects
# import boto3
# s3 = boto3.resource('s3')
# for bucket in s3.buckets.all():
#     print(bucket.name)


# -----list iam users
# import boto3

# client= boto3.client('iam') 
# response = client.list_users()
# for x in response['Users']:
#   print(x['UserName'])


#------update access key status to inactive which cross certain days

import boto3
from datetime import datetime, timezone


client = boto3.client("iam")
# sns = boto3.resource("sns")
paginator = client.get_paginator('list_users')
current_date=datetime.now(timezone.utc)
max_key_age=200

for response in paginator.paginate():
    for user in response['Users']:      #print(user)
        username = user['UserName']
        listkey = client.list_access_keys(UserName=username)
        print(listkey, end="\n")
        for accesskey in listkey['AccessKeyMetadata']:
            accesskey_id = accesskey['AccessKeyId']
            key_creation_date = accesskey['CreateDate']
            age = (current_date - key_creation_date).days
            # age = (current_date - key_creation_date)
            # print(age) 

            if age > max_key_age:
                print("Deactivating Key for the following users: " + username)
                client.update_access_key(UserName=username, AccessKeyId=accesskey_id, Status='Inactive')


#-----change access key for age crossed users using CLI

# aws iam list-access-keys --user-name shweta
# aws iam create-access-key --user-name shweta
# aws iam list-access-keys --user-name shweta
# aws iam update-access-key --access-key-id AKIAI44QH8DHBEXAMPLE --status Inactive --user-name shweta
# aws iam list-access-keys --user-name shweta
# aws iam delete-access-key --access-key-id AKIAI44QH8DHBEXAMPLE --user-name shweta
# aws iam list-access-keys --user-name shweta



